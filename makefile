
pdf:
	xelatex treecompare.tex

all:
	xelatex treecompare.tex
	bibtex treecompare
	xelatex treecompare.tex
	xelatex treecompare.tex

clean: 
				rm -f *.aux *.log *.bbl *.blg *.brf *.cb *.ind *.idx *.ilg	\
				*.inx *.toc *.out 

.PHONY : pdf clean all
