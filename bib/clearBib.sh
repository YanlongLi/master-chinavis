#!/bin/bash

# sed -e '/^\s*annote/d' -e '/^\s*file/d' -e '/^\s*abstract/d' -e '/^\s*url/d' -e '/^\s*(doi|isbn|issn)/d' -i Reffed.bib

sed -e '/^\s*\(pmid\|keywords\|annote\|file\|abstract\|url\|doi\|isbn\|issn\|address\|month\)/d' -i Reffed.bib
